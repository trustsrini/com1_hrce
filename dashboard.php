<?php 
include "includes/header.php";
include "includes/left-menu.php";
include "includes/top_menu.php";
?>

<!-- START PAGE CONTENT -->
<div class="content">
    <div class="container-fixed-lg">
        <div class="row no-margin padding-10">
            <div class="col-md-3 padding-10">
                <a href="customer_list.php">
                    <div class="bg-primary text-center padding-10">
                        <i class="fa fa-ticket pull-left fs-60 text-white padding-10"></i>
                        <h4 class="text-white no-margin">Tokens</h4>
                        <h1 class="text-white no-margin semi-bold">642</h1>
                    </div>
                </a>
            </div>
            <div class="col-md-3 padding-10">
                <a href="fooditem_list.php">
                    <div class="bg-complete text-center padding-10">
                        <i class="fa fa-users pull-left fs-60 text-white padding-10"></i>
                        <h4 class="text-white no-margin">Persons</h4>
                        <h1 class="text-white no-margin semi-bold">4600</h1>
                    </div>
                </a>
            </div>
            <!-- <div class="col-md-3 padding-10">
                <a href="sales_orders_list.php">
                    <div class="bg-success text-center padding-10">
                        <i class="fa fa-shopping-cart pull-left fs-60 text-white padding-10"></i>
                        <h4 class="text-white no-margin">Orders</h4>
                        <h1 class="text-white no-margin semi-bold">329</h1>
                    </div>
                </a>
            </div> -->
        </div>
    </div>
</div>
<!-- END PAGE CONTENT -->

<?php
include "includes/footer.php";
?>