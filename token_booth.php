<?php 
include "includes/header.php";
// include "includes/left-menu.php";
include "includes/top_menu.php";
?>

<style>
.camera-view {
    height: 400px;
    width: 100%;
    border: 1px solid rgba(0, 0, 0, 0.07);
    margin-bottom: 20px;
    position: relative;
    background: #f4f4f4;
    text-align: center;
}

body.menu-pin .page-container .page-content-wrapper .content {
    padding-left: 0;
}

.top-title {
    margin-left: 20px;
}

.topLeft {
    display: flex;
    align-items: center;
}

.callBoothCenter {
    display: flex;
    align-items: center;
    justify-content: center;
    height: calc(100vh - 250px);
}

.captureBtn {
    position: absolute;
    bottom: 0;
    text-align: center;
    width: 100%;
    padding: 10px;
}

.camera-view img {
    width: 100%;
    height: 100%;
    object-fit: none;
}

.booth-logo {
    display: block;
}

.footer-btn {
    display: flex;
    justify-content: space-between;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php 

    $connection = dbConnection();

    mysqli_set_charset($connection, 'utf8');
    
    $details_query_exe = mysqli_query($connection,"SELECT * FROM `registration`");

    $details = mysqli_fetch_object($details_query_exe);

?>

<!-- START PAGE CONTENT -->
<div class="content">
    <!-- START PAGE CONTAINER -->
    <div class="container-fixed-lg">
        <!-- START PANEL -->
        <div class="panel panel-default no-border">
            <div class="bg-master-lightest border-b">
                <div class="panel-heading padding-10 p-r-20 p-l-20">
                    <div class="panel-title fs-16 p-t-10 text-black">
                        <?php echo $details -> temple_name . " - ". $details -> temple_city . " - ".$details -> temple_pin ?>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>

            <!-- START PANEL BODY-->
            <div class="panel-body">
                <div class="callBoothCenter">
                    <div class="text-center">
                        <div class="text-center">
                            <h5 class="font-montserrat bold fs-20">CALL TO</h5>
                            <h1 class="font-montserrat fs-60 bold "><?php echo $details -> mobile_no ?></h1>
                        </div>
                        <div class="padding-20 text-center">
                            <!-- <button type="button" class="btn btn-submit btn-cons " data-toggle="modal"
                                data-target="#callModal">Open</button> -->
                            <a type="button" class="btn btn-submit btn-cons" href="manual_token.php"
                                target="_blank">Manual token</a>
                            <a type="button" class="btn btn-submit btn-cons" data-toggle="modal"
                                data-target="#tokenView">Register</a>
                            <a type="button" class="btn btn-submit btn-cons" href="javascript:syncLog()">Sync</a>
                        </div>

                        <!-- <a href="token_list.php">Back to Token</a> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END PANEL BODY-->
    </div>
    <!-- END PANEL -->
</div>
<!-- END PAGE CONTAINER -->

<div class="modal fade slide-up disable-scroll" id="callModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content" id="tokenbooth-content">

            </div>
        </div>
    </div>
</div>
<div class="modal fade slide-up disable-scroll" id="registerModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content" id="register-content">

            </div>
        </div>
    </div>
</div>

<div class="modal fade slide-up disable-scroll" id="tokenView" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-center border-b">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                    </button>
                    <h5>Token <span class="semi-bold">Register</span></h5>
                </div>
                <div class="modal-body p-t-25">
                    <div class="form-group" id="token_register_form">
                        <label>Id</label>
                        <input type="text" id="temple_id" class="form-control required"
                            value='<?php echo $details -> temple_id ?>'>
                    </div>
                    <div class="form-group">
                        <label>Counter</label>
                        <input type="text" id="counter_id" class="form-control required"
                            value='<?php echo $details -> counter_id ?>'>
                    </div>
                    <div class="form-group">
                        <label>Temple Name</label>
                        <input type="text" id="temple_name" class="form-control required"
                            value='<?php echo $details -> temple_name ?>'>
                    </div>
                    <div class="form-group">
                        <label>Temple City</label>
                        <input type="text" id="temple_city" class="form-control required"
                            value='<?php echo $details -> temple_city ?>'>
                    </div>
                    <div class="form-group">
                        <label>Temple Pin</label>
                        <input type="text" id="temple_pin" class="form-control required"
                            value='<?php echo $details -> temple_pin ?>'>
                    </div>
                    <div class="form-group">
                        <label>Mobile No</label>
                        <input type="text" id="mobile_no" class="form-control required"
                            value='<?php echo $details -> mobile_no ?>'>
                    </div>
                    <div class="form-group">
                        <label>Sync url</label>
                        <input type="text" id="sync_url" class="form-control required"
                            value='<?php echo $details -> sync_url ?>'>
                    </div>
                    <?php  
                    $count = mysqli_num_rows($details_query_exe);
                    if($count >=1){
                    ?>
                    <div>
                        <button class="btn btn-sm btn-info"
                            onclick="update_token(<?php echo $details -> id ?>)">Update</button>
                    </div>
                    <?php }else{ ?>
                    <div>
                        <button class="btn btn-sm btn-info" onclick="insert_token()">Insert</button>
                    </div>
                    <?php } ?>



                </div>
            </div>
        </div>
    </div>
</div>
<?php
    include "includes/footer.php";
    include "event_controller/listener.php";
?>
<script src="assets/js/token.js" type="text/javascript"></script>
<script>
function syncLog() {

    $.ajax({
        url: 'cron/sync.php',
        type: "POST",
        success: function() {
            location.reload();
        }
    })


}
</script>