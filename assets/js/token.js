$("#camera_video").hide();
function openCallPanel(mobile){
  // mobile = "7339650900";
    $.post("token_modal.php", { mobile : mobile } ,function(data, status){
        $('#tokenbooth-content').html(data);
        $('#callModal').modal("show");
    });
}

function generateToken(){
  if(validateRequiredByClass("mandatory")) {
    $.post("generate_token.php", $("#candidate_details").serialize() ,function(data, status){
      var res = JSON.parse(data);  
      if(res.status){
        $('#tokenbooth-content').html("");
        $('#callModal').modal("hide");
        window.open(`token.php?token=${res.data}`, '_blank');
        location.reload();
      } else alert(res.message);
    });
  } else {
    alert("Please check the highlighted fields");
  }
}

function update_token(id){

  var temple_id = $('#temple_id').val();

  var counter_id = $('#counter_id').val();

  var temple_name = $('#temple_name').val();

  var temple_city = $('#temple_city').val();

  var temple_pin = $('#temple_pin').val();

  var mobile_no = $('#mobile_no').val();

  var sync_url = $('#sync_url').val();

  var error = false;
          
  $('.required').each(function() {

if($(this).val() == ""){

  error = true;

     $(this).css('border','1px solid red');
    
     alert('please filled highlighted fields');
 
}else{
     error = false;
} 
     });

     if(!error){
            
             $.ajax({
                      url:"token_register.php",
                      type:"POST",
                      data:{
                        id:id,
                        temple_id:temple_id,
                        counter_id:counter_id,
                        temple_name:temple_name,
                        temple_city:temple_city,
                        temple_pin:temple_pin,
                        mobile_no:mobile_no,
                        sync_url:sync_url

                      },
                      success:function(data){
                        var response = JSON.parse(data);

                        if(response.status){

                             alert(response.message)

                               location.reload();

                        }else{

                                alert(response.message);
                        }
                      }
             })
     }
}

function insert_token(){

  var temple_id = $('#temple_id').val();

  var counter_id = $('#counter_id').val();

  var temple_name = $('#temple_name').val();

  var temple_city = $('#temple_city').val();

  var temple_pin = $('#temple_pin').val();

  var mobile_no = $('#mobile_no').val();

  var sync_url = $('#sync_url').val();

  var error = false;
          
  $('.required').each(function() {

if($(this).val() == ""){

  error = true;

     $(this).css('border','1px solid red');
    
     alert('please filled highlighted fields');
 
}else{
     error = false;
} 
     });

     if(!error){
            
             $.ajax({
                      url:"token_register.php",
                      type:"POST",
                      data:{

                        temple_id:temple_id,
                        counter_id:counter_id,
                        temple_name:temple_name,
                        temple_city:temple_city,
                        temple_pin:temple_pin,
                        mobile_no:mobile_no,
                        sync_url:sync_url

                      },
                      success:function(data){
                        var response = JSON.parse(data);

                        if(response.status){

                             alert(response.message)

                               location.reload();

                        }else{

                                alert(response.message);
                        }
                      }
             })
     }
}