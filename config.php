<?php
    
    session_start();
    date_default_timezone_set('Asia/Kolkata');

    function getPageName() {
        
        return basename($_SERVER['PHP_SELF']);
    
    }

    function dbConnection() {

        $server = "localhost";
        
        $user_name = "root";
        
        $password = "#*1234Abcd*#";
        
        $database = "com1_hrnc";

        $connection = mysqli_connect($server, $user_name, $password, $database) or die(mysqli_error($connection));
        
        return $connection;
    
    }

    function getLastArrayValueofKey($array,$array_key){

        $array_count = count($array);
        
        return $array[$array_count - 1] -> {$array_key};
        
    }

    function searchObjectArray($array,$key,$value) {
        
        $response = array();

        foreach($array as $object) {

            if($object -> {$key} == $value){

                $response = $object;
                
            }

        }

        return $response;

    }

    function cleanString($string) {
        
        return trim(preg_replace( "/\r|\n/", "", $string));
    
    }

?>