<!-- BEGIN SIDEBAR -->
<div class="page-sidebar" data-pages="sidebar">
    <div id="appMenu" class="sidebar-overlay-slide from-top">
</div>

    <!-- BEGIN SIDEBAR HEADER -->
    <div class="sidebar-header">
            <img src="assets/img/logo.png" width="160px" >
        <div class="sidebar-header-controls">
        </div>
    </div>
    <!-- END SIDEBAR HEADER -->

    <!-- BEGIN SIDEBAR MENU -->
    <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="m-t-30">
                <a href="dashboard.php">
                    <span class="title">Dashboard</span>
                </a>
                <span class="<?php if($pageName=='dashboard.php') {echo "bg-primary";} ?> icon-thumbnail">
                <i class="fa fa-dashboard"></i>
                </span>
            </li>
            <li>
                <a href="token_list.php">
                    <span class="title">Token</span>
                </a>
                <span class="<?php if($pageName=='token_list.php' || $pageName=='token_booth.php') {echo "bg-primary";} ?> icon-thumbnail ">
                <i class="fa fa-ticket"></i>
                </span>
            </li>
            <li>
                <a href="token_list.php">
                    <span class="title">Token</span>
                </a>
                <span class="<?php if($pageName=='token_list.php' || $pageName=='token_booth.php') {echo "bg-primary";} ?> icon-thumbnail ">
                <i class="fa fa-ticket"></i>
                </span>
            </li>
            <!-- <li>
                <a href="token_add.php">
                    <span class="title">Token Both</span>
                </a>
                <span class="<?php if($pageName=='token_add.php' || $pageName=='customer_add.php' || $pageName=='customer_edit.php') {echo "bg-primary";} ?> icon-thumbnail ">
                <i class="fa fa-building"></i>
                </span>
            </li> -->
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->

</div>
<!-- END SIDEBAR -->