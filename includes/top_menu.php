
<div class="page-container">
    <!-- START HEADER -->
    <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
            <!-- LEFT SIDE -->
            <div class="pull-left full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                    <a href="#"
                        class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5"
                        data-toggle="sidebar">
                        <span class="icon-set menu-hambuger"></span>
                    </a>
                </div>
                <!-- END ACTION BAR -->
            </div>
            <div class="pull-center hidden-md hidden-lg">
                <div class="header-inner">
                    <div class="brand inline">
                        <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png"
                            data-src-retina="assets/img/logo_2x.png" width="78" height="15">
                    </div>
                </div>
            </div>
            <!-- RIGHT SIDE -->
            <div class="pull-right full-height visible-sm visible-xs">
                <!-- START ACTION BAR -->
                <div class="header-inner">
                    <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview"
                        data-toggle-element="#quickview">
                        <span class="icon-set menu-hambuger-plus"></span>
                    </a>
                </div>
                <!-- END ACTION BAR -->
            </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm top-title">
            <div class="header-inner">
                <!-- <div class="brand inline">
                    <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png"
                        data-src-retina="assets/img/logo_2x.png" width="78" height="22">
                </div> -->
                <div class="topLeft">
                <img src="assets/img/lemon-logo.png" alt="logo" height="46px" class="m-r-15 booth-logo" >
                        <div>
                        <?php 
                             include_once "../config.php";

                              $connection = dbConnection();

                               mysqli_set_charset($connection, 'utf8');

                                $query = "SELECT * FROM `registration` WHERE id='1'";

                                $details_query_exe = mysqli_query($connection,$query);

                                $details = mysqli_fetch_object($details_query_exe);



?>         
                <p class="m-b-0 bold">தமிழ்நாடு அரசு</p>
                <h4 class="bold color-primary m-b-0 m-t-0 fs-18" >இந்து சமய அறநிலையத் துறை</h4>
                </div>
                </div>
            </div>
        </div>

        <div class=" pull-right">
            <!-- START User Info-->
            <div class="visible-lg visible-md m-t-10">
            <?php 

                  $count = "CALL usb_local_dashboard()";

                  $count_query_exe = mysqli_query($connection,$count);

                  $count_details = mysqli_fetch_object($count_query_exe);
                  

            ?>    
            <div class="pull-left m-t-5 btn-group">
                    <div class="number-box bg-primary-dark m-r-10">Today - <?php echo $count_details ->CntToday ?></div>
                    <div class="number-box bg-success-dark m-r-10">7 Days - <?php echo $count_details ->Cnt7 ?></div>
                    <div class="number-box bg-primary m-r-10">30 Days - <?php echo $count_details ->Cnt30 ?></div>
                    <div class="number-box bg-danger m-r-10">Unsync - <?php echo $count_details ->DataUnsync ?></div>
                </div>
                <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
                    <span class="semi-bold">Counter - </span><span class="text-master"><?php echo $details -> counter_id ?></span>
                </div>
                <div class="dropdown pull-right">
                    <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <span class="thumbnail-wrapper d32 circular inline m-t-5">
                            <img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg"
                                data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
                        </span>
                    </button>
                    <ul class="dropdown-menu profile-dropdown" role="menu">
                        <li><a href="profile.php"><i class="fa fa-user"></i> Profile</a>
                        <li class="bg-master-lighter">
                            <a href="index.php" class="clearfix">
                                <span class="pull-left">Logout</span>
                                <span class="pull-right"><i class="pg-power"></i></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END User Info-->
        </div>
    </div>
    <!-- END HEADER -->
    <div class="page-content-wrapper bg-white">