<?php 

    require 'config.php';

    extract($_POST);

    $connection = dbConnection();

    $register_user_exe = mysqli_query($connection,"CALL usp_registration_entry('$mobile','$no_of_persons')") or die(mysqli_error($connection));

    $token = mysqli_fetch_object($register_user_exe) -> response;

    $file_path = "uploads/".date('Ymd');

    if(!file_exists($file_path)) mkdir($file_path);

    if($token != ""){

        file_put_contents($file_path."/".$token.".png", file_get_contents($candidate_image));

        echo json_encode(array("status" => true, "message" => "","data" => $token));

    } else echo json_encode(array("status" => false, "message" => "Error","data" => ""));

?>