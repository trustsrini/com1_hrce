<!-- to enable the service -->

systemctl enable tnhrce-eventcontroller.service

<!-- to start the service -->

systemctl start tnhrce-eventcontroller.service

<!-- to check the status of the service -->

systemctl status tnhrce-eventcontroller.service

<!-- to stop the service -->

systemctl stop tnhrce-eventcontroller.service
