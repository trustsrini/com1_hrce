#!/usr/bin/sh

cd /var/www/html/tnhrce/event_controller

npm i

cp /var/www/html/tnhrce/event_controller/daemon/tnhrce-eventcontroller.service /etc/systemd/system/tnhrce-eventcontroller.service

systemctl enable tnhrce-eventcontroller.service

systemctl start tnhrce-eventcontroller.service

# systemctl status tnhrce-eventcontroller.service