<?php

    include __DIR__.'/config.php';
    
    ini_set("default_socket_timeout", -1);

    include("vendor/autoload.php");
    
    use ElephantIO\Client;
    
    use ElephantIO\Engine\SocketIO\Version2X;

    $version = new Version2X($listener_config['events_ip']);

    $client = new Client($version);
    
    if(isset($_REQUEST)){

        $event = array();

        $request_data = $_REQUEST;

        if(isset($_REQUEST['hrnc-missedcall']) && $_REQUEST['hrnc-missedcall']){

            if(strlen($_REQUEST['mobile']) >= 10) {

                $event = array(
                    
                    "event" => "missed-call",
    
                    "event_data" => array(
    
                        "data_mobile" => $_REQUEST['mobile']
                    
                    ),
                    
                );

            }
                        
        }

        if(!empty($event)){

            $client -> initialize();
            
            $client -> emit("event",$event);
    
            $client -> close();

        }

    }

?>