var socket = require("socket.io"),
    express = require("express"),
    http = require("http");

var app = express();
var http_server = http.createServer(app).listen(3003);

if (http_server) {

    console.log("listening on port 3003...");

    var io = socket.listen(http_server);
    io.sockets.on("connection", function (socket) {
        socket.on("event", function (data) {
            io.emit("missed-call", data);
        });
    });

}