<?php include_once __DIR__."/config.php"; ?>
<script src="event_controller/assets/js/socket.io.js"></script>
<input id='events_ip' value="<?php echo $listener_config['events_ip'] ?>" hidden />
<script>

    $(document).ready(function() {
        var socket = io.connect("<?php echo $listener_config['events_ip'] ?>");
        socket.on("missed-call", function(e) {
            console.log(e);
            const { event,event_data } = e;
            console.log(event_data);
            var mobile = event_data['data_mobile'];
            openCallPanel(mobile);
        });
    });
    
</script>