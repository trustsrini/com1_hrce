<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<!-- <script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.5/dist/JsBarcode.all.min.js"></script> -->
<script src="assets/plugins/JsBarcode.all.min.js" type="text/javascript"></script>
<div id="divToPrint">
<?php 

    include 'config.php';

    $connection = dbConnection();

    mysqli_set_charset($connection, 'utf8');

    $token_no = $_GET['token'];

    $file_path = "uploads/".date('Ymd');

    $get_token_details_exe = mysqli_query($connection,"CALL usp_generate_manualtokens()") or die(mysqli_error($connection));

    while($row = mysqli_fetch_object($get_token_details_exe)) {

        ?>
            <div style="font-size: 12px; text-align:center;margin-top: -10px; width:200px;margin:0 auto" >
            <p style="margin:0px;font-size:16px"><b><?php echo $row -> TempleName ?></b></p>
            <p style="margin:0px;margin-bottom:20px;"><?php echo $row -> TempleCity ?></p>
            
            
            <p style="margin:0;text-align:center;font-size:16px;margin-top:20px"><b>இலவச முடி காணிக்கை</b></p>
            <div style="text-align:center;margin-top:20px;margin-left:0px;">
            <svg style="margin-left:0px;" class="barcode"
                jsbarcode-format="CODE128"
                jsbarcode-value="<?php echo $row -> token_code; ?>"
                jsbarcode-textmargin="0"
                jsbarcode-width="1"
                jsbarcode-height="30"
                jsbarcode-fontSize="16"
                jsbarcode-marginRight="20"
                jsbarcode-flat = "true"
                >
            </svg>
            <!-- <img src="https://bit.ly/3k6fGCe" alt="barcode"  /> -->
            </div>
            <div style="text-align:center;"> - </div>
            <p style="margin:0;text-align:left;font-size:16px;margin-bottom:40px;">Mobile : </p>
            </div>
            <hr>
        <?php
        ?>
        <script>
            JsBarcode(".barcode").init();
        </script>
<?php

    }

?>
</div>

<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    function printContent(){
        var win = window.open('','','left=0,top=0,width=552,height=477,toolbar=0,scrollbars=0,status =0');
        var content = "<html>";
        content += "<body onload=\"win.print();window.close();\">";
        content += document.getElementById("divToPrint").innerHTML ;
        content += "</body>";
        content += "</html>";
        win.document.write(content);
        setTimeout(() => {
            win.print();
            win.close();
            window.close();
        }, 1000);
    }
    $(function() {
        printContent();
    });
</script>
</html>