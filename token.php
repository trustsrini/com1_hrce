<html >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<script src="assets/plugins/JsBarcode.all.min.js" type="text/javascript"></script>
<div id="divToPrint">
<?php 

    header('Content-Type: text/html;charset=UTF-8');

    include 'config.php';

    $connection = dbConnection();

    $token_no = $_GET['token'];

    $file_path = "uploads/".date('Ymd');

    mysqli_set_charset($connection, 'utf8');

    $get_token_details_exe = mysqli_query($connection,"CALL usp_registration_print('$token_no')") or die(mysqli_error($connection));
    while($row = mysqli_fetch_object($get_token_details_exe)) {

        ?>
            <div style="font-size: 12px; text-align:center;margin-top: -10px; width:200px;margin:0 auto" >
            <p style="margin:0px;font-size:16px"><b><?php echo $row -> TempleName ?></b></p>
            <p style="margin:0px;margin-bottom:20px;"><?php echo $row -> TempleCity ?></p>
            <p style="margin:0px; text-align:center; font-family:arial;margin-bottom:10px;"><?php echo date("d.m.Y h:i A",strtotime($row -> visit_datetime)) ?></p>
            
            <p style="margin:0;text-align:center;font-size:16px;margin-top:20px"><b>இலவச முடி காணிக்கை</b></p>
            <div style="text-align:center;margin-top:20px;">
            <svg style="margin-left:0px;" class="barcode"
                jsbarcode-format="CODE128"
                jsbarcode-value="<?php echo $row -> reg_ref; ?>"
                jsbarcode-textmargin="0"
                jsbarcode-width="1"
                jsbarcode-height="30"
                jsbarcode-fontSize="16"
                jsbarcode-marginRight="20"
                jsbarcode-flat = "true"
                >
            </svg>
            <!-- <img src="https://bit.ly/3k6fGCe" alt="barcode"  /> -->
            </div>
            <div style="text-align:center;">
            <img src="<?php echo $file_path."/".$token_no.".png" ?>" alt="photo" style="width:180px;height:auto;object-fit:contain;border:0px solid #e2e2e2;margin-top:20px;margin-bottom:20px;" />
            </div>
            <p style="margin:0;text-align:center;font-size:16px;margin-bottom:20px;"><?php echo $row -> mobile ?></p>
            </div>
            <hr>
            <script>
            JsBarcode(".barcode").init();
           </script>
        <?php
        
    }

?>
</div>
</html>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script>
    function printContent(){
        var win = window.open('','','left=0,top=0,width=552,height=477,toolbar=0,scrollbars=0,status =0');
        var content = "<html>";
        content += "<body onload=\"win.print();window.close();\">";
        content += document.getElementById("divToPrint").innerHTML ;
        content += "</body>";
        content += "</html>";
        win.document.write(content);
        setTimeout(() => {
           win.print();
           win.close();
           window.close();
        }, 1000);
    }
    $(function() {
        printContent();
    });
</script>
