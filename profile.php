<?php 
include "includes/header.php";
include "includes/left-menu.php";
include "includes/top_menu.php";
?>

<!-- START PAGE CONTENT -->
<div class="content">
    <div class="container-fluid no-padding">
        <!-- START PANEL -->
        <div class="panel panel-transparent no-border">
            <div class="bg-master-lightest border-b">
                <div class="panel-heading padding-10 p-r-20 p-l-20">
                    <div class="panel-title fs-16 p-t-10 text-black">Profile</div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="panel-body">
                <ul class="nav nav-tabs nav-tabs-fillup bg-master-lightest" data-init-reponsive-tabs="dropdownfx">
                    <li class="active">
                        <a data-toggle="tab" href="#tab-profile"><span class="align-middle">Info</span></a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#tab-password-manage"><span class="align-middle">Password
                                Manage</span></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-profile">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input type="text" class="form-control" name="" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Mobile No</label>
                                    <input type="number" class="form-control" name="" id="" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-password-manage">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" class="form-control" name="" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control" name="" id="" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="" id="" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-master-lightest padding-20">
                <a role="button" href="daily-homework-list.php" type="button" class="btn btn-submit  btn-cons"
                    data-dismiss="modal">Submit</a>
                <a role="button" href="daily-homework-list.php" type="button" class="btn btn-danger btn-cons"
                    data-dismiss="modal">Cancel</a>
            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>

<?php
include "includes/footer.php";
?>