<?php 
include "includes/header.php";
include "includes/left-menu.php";
include "includes/top_menu.php";
?>

<!-- START PAGE CONTENT -->
<div class="content">
    <div class="container-fluid no-padding">
        <!-- START PANEL -->
        <div class="panel panel-transparent">
            <div class="bg-master-lightest border-b">
                <div class="panel-heading padding-10 p-r-20 p-l-20">
                    <div class="panel-title fs-16 p-t-10 text-black">Token</div>
                    <div class="pull-right">
                        <a href="token_booth.php">
                            <button class="btn btn-primary btn-sm" type="button">Token Booth</button>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped" id="tableWithSearchExportOptions">
                    <thead>
                        <tr>
                            <th>Token No.</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Persons</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>TN0001</td>
                            <td>Vinoth Kumar R</td>
                            <td>9123456789</td>
                            <td>6</td>
                            <td class="text-center">
                                <button type="button" title="Edit" class="btn btn-warning" data-toggle="modal"
                                    data-target="#tokenView">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <!-- <button type="button" title="Delete" class="btn btn-danger"><i
                                        class="fa fa-trash-o"></i></button> -->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END PANEL -->
</div>
<!-- END PAGE CONTENT -->

<div class="modal fade slide-up disable-scroll" id="tokenView" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-center border-b">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                            class="pg-close fs-14"></i>
                    </button>
                    <h5>Token <span class="semi-bold">Info</span></h5>
                </div>
                <div class="modal-body p-t-25">
                    <div class="form-group">
                        <p><span class="bold sm-p-t-20">Token No.</span> : TN001</p>
                    </div>
                    <div class="form-group">
                        <p><span class="bold sm-p-t-20">Token No.</span> : Vinoth Kumar R</p>
                    </div>
                    <div class="form-group">
                        <p><span class="bold sm-p-t-20">MOBILE</span> : 9123456789</p>
                    </div>
                    <div class="form-group">
                        <p><span class="bold sm-p-t-20">PERSONS</span> : 6</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include "includes/footer.php";
?>