<?php 
    include __DIR__.'/../config.php';

    header('Content-Type: application/json');

    $connection = dbConnection();

    $data = file_get_contents('php://input');
    
    $inputData = json_decode($data);

    $temp = array();

    $query = "SELECT * FROM `visitor_log` WHERE upload_flag = 0 LIMIT 0,100";

    $query_exe = mysqli_query($connection, $query);

    $rows = array();

    $tokens = array();

    while($row = mysqli_fetch_object($query_exe)) {

        $rows[] = $row;

        $tokens[] = "'".$row -> reg_ref."'";

    }
    
    $details_query_exe = mysqli_query($connection,"SELECT * FROM `registration`");

    $details = mysqli_fetch_object($details_query_exe);

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_POST, 1);
    
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(array("rows" => $rows)));
    
    curl_setopt($curl, CURLOPT_URL, $details->sync_url);
    
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type' => 'application/json'));
    
    $result = curl_exec($curl);

    curl_close($curl);
    
    $response = json_decode($result);

    if($response -> status === true) {

        $query = "UPDATE visitor_log SET upload_flag = '1' WHERE `reg_ref` IN (".implode(',',$tokens).")";

        mysqli_query($connection,$query);

    }

    $cron_logs = file_get_contents("cron_log.json") != "" ? json_decode(file_get_contents("cron_log.json")) : array();

    array_push($cron_logs, array("time" => date("Y-m-d h:s:i A"), "synced_tokens" => $tokens, "response" => $response));

    file_put_contents("cron_log.json", json_encode($cron_logs));

?>