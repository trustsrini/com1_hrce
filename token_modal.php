<div class="modal-body p-t-25">
    <center><h4><?php echo $_POST['mobile'] ?></h4></center>
    <div class="camera-view">
        <img src="assets/img/avatar.png" id="preview_photo"/>
        <video id="camera_video" style="height: 100%;width: 100%">Video stream not available.</video>
        <div class="captureBtn">
            <button class="btn btn-primary btn-cons m-r-0" id="start-camera-btn" onclick="startCamera()">Start Camera</button>
            <button class="btn btn-primary btn-cons m-r-0" id="capture-btn">Capture</button>
            <button class="btn btn-primary btn-cons m-r-0" id="re-capture-btn" onclick="startCamera()">Retake</button>
        </div>
        <canvas id="canvas" style="display: none;"></canvas>
    </div>
    <form id="candidate_details">
        <input class="mandatory" type="text" name="candidate_image" id="candidate_image" hidden>
        <input class="mandatory" name="mobile" id="mobile" value="<?php echo $_POST['mobile'] ?>" hidden>
        <div class="form-group">
            <label for="no_of_persons">Number of Persons</label>
            <select class="form-control mandatory" id="no_of_persons" name="no_of_persons">
                <option value="1" selected>1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
    </form>
    <div class="footer-btn">
    <button class="btn btn-submit btn-cons round" id="generate-token-btn" onclick="generateToken()" disabled>Save & Print</button>
    <button class="btn btn-danger btn-cons" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</div>

<script>
    $("#capture-btn").hide();
    $("#re-capture-btn").hide();
    function startCamera(){
        $("#re-capture-btn").hide();
        $("#start-camera-btn").hide();
        $("#capture-btn").show();
        startup();
    }
    
</script>